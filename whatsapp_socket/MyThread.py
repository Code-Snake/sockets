from threading import Thread
from typing import Any


class MyThread(Thread):

    def __init__(self, group=None, target=None, name=None, args=(), kwargs=None):
        self._target = None
        self._args = None
        self._kwargs = None
        kwargs = {} if kwargs is None else kwargs
        Thread.__init__(self, group, target, name, args, kwargs)
        self._return = None

    def run(self):
        if self._target is not None:
            self._return = self._target(*self._args, **self._kwargs)

    def join(self, *args) -> Any:
        Thread.join(self, *args)
        return self._return
