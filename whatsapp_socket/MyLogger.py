from colorama import Fore
class MyLogger:
    @staticmethod
    def HTTP(text):
        print(f"{Fore.LIGHTBLUE_EX}API{Fore.WHITE}:     {text}")

    @staticmethod
    def WSS(text):
        print(f"{Fore.LIGHTGREEN_EX}For_user{Fore.WHITE}:     {text}")