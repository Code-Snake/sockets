class ErrorCodes:

    codes = {
        100: 'Неизвестная ошибка',
        101: 'Тело обращения должно быть в формате JSON',
        102: 'В теле обращения ожидалось поле %1%',
        103: 'Пользователь с данным uuid: %1%, не найден',
        104: 'Вы уже вошли в диалог %1%',
        105: 'У Вас не достаточно прав',
        106: 'Вы не авторизованы',
        107: 'Вас нет в диалоге %1%',
        108: 'Диалога %1% не существует',
        109: 'Пользователь не авторизован в чате',
        110: 'Неизвестный тип сообщения: %1%',
        111: 'Неизвестный статус сообщения: %1%',
        112: 'Не удалось отправить сообщение',
        113: 'Сообщение %1% не найдено',
        114: 'Вы не можете установить метку Просмотрено на свои сообщения или сообщения сотрудников',
        115: 'Вы уже авторизованы в чате',
        116: 'Неизвестная команда: %1%',
        117: 'Вы не вошли в диалог',
        118:'who are you?',
    }

    @staticmethod
    def Get(code, *replace):

        if code not in ErrorCodes.codes:
            return ErrorCodes.Get(100)

        error_message = ErrorCodes.codes[code]

        count = 1

        for value in replace:
            error_message = error_message.replace(f"%{count}%", value)
            count += 1

        return {
            'code': code,
            'message': error_message
        }