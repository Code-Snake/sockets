import asyncio
from hypercorn.config import Config
from hypercorn.asyncio import serve
from sockets import WhatsappChat

config = Config()
config.bind = '0.0.0.0:5555'

asyncio.run(serve(WhatsappChat.api, config))
