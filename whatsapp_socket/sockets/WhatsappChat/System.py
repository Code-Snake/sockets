from fastapi import WebSocket

class System:

    def __init__(self,  socket: WebSocket = None, password: str = None, _system: bool = False):
        self._socket = socket
        self._password = password
        self._is_admin = False

    def GetSocket(self) -> WebSocket:
        return self._socket

    def GetPassword(self) -> str:
        return self._password
