import asyncio
from fastapi import WebSocket
from ErrorCodes import ErrorCodes
from MyThread import MyThread
from MyLogger import MyLogger
from sockets.WhatsappChat.User import User
from sockets.WhatsappChat.System import System

users = []
systems = []

class SocketHandler:
    def __init__(self):
        self._logger = MyLogger

    def Auth(self, socket: WebSocket, body):

        message_type = body['type']

        if message_type == 'system':
            if 'password' not in body:
                self.SendMessage(socket, ErrorCodes.Get(102, 'password'))
                return False
            systems.append(self.CreateSystem(socket, body).GetSocket())
            return True

        if message_type == 'user':
            if 'token' not in body:
                self.SendMessage(socket, ErrorCodes.Get(102, 'token'))
                return False
            users.append(self.CreateUser(socket, body).GetSocket())
            return True

        self.SendMessage(socket, ErrorCodes.Get(118))

        return False

    def ReceiveMessage(self, socket: WebSocket, body: dict, users ):

        if socket in users:
            return

        if 'type' not in body and 'event' not in body:
            self.SendMessage(socket, ErrorCodes.Get(102, 'type or event'))
            return

        if 'event' in body:
            self.Admin(socket, body)

    def Admin(self, socket, body):

        if body['event'] == "new_message":
            new_message = {"event": "new_message", "payload": body["payload"]}
            for i in range(0, len(users)):
                self.SendMessage(users[i], new_message)
            return

        if body['event'] == "new_dialog":
            new_dialog = {"event": "new_dialog", "payload": body["payload"]}
            for i in range(0, len(users)):
                self.SendMessage(users[i], new_dialog)
            return

        self.SendMessage(socket, ErrorCodes.Get(110, body['event']))

    def SendMessage(self, member, body: dict):

        try:
            thread = MyThread(target=asyncio.run, args=(member.send_json(body),))
            thread.start()
            thread.join()
            self._logger.WSS(f" {body}")
        except Exception:
            print("Ошибка отправки сообщения")

    def CreateUser(self, socket : WebSocket, body):

        user = User(socket, body['token'])
        return user

    def CreateSystem(self, socket : WebSocket, body):

        system = System(socket, body['password'])
        return system
