from fastapi import WebSocket

class User:

    def __init__(self,  socket: WebSocket = None, token: str = None, _system: bool = False):
        self._socket = socket
        self._token = token
        self._is_admin = False

    def GetSocket(self) -> WebSocket:
        return self._socket

    def GetToken(self) -> str:
        return self._token
