import os
import sys
from sockets.WhatsappChat.SocketHandler import SocketHandler, users, systems
import json
from fastapi import FastAPI, WebSocket
from fastapi.middleware.cors import CORSMiddleware

sys.path.insert(0, os.path.abspath('../../'))
sys.path.insert(0, os.path.abspath('/'))

socketHandler = SocketHandler()
api = FastAPI(docs_url=None, redoc_url=None)

api.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@api.websocket("/v1/whatsapp/chat/ws")
async def WhatsappSocket(socket: WebSocket):

    await socket.accept()


    print(f"Принято подключение [{socket.client.host}:{socket.client.port}]")

    while True:

        try:
            text = await socket.receive_text()
            print(f"Получено сообщение {text}")
            if 'type' in text:
                if not(socketHandler.Auth(socket, json.loads(text))): #проверка,что подсоединился юзер/система с верными данными
                    await socket.close()
                    print(f"Разорвано соединение [{socket.client.host}:{socket.client.port}]")
                    return
            socketHandler.ReceiveMessage(socket, json.loads(text), users) #обработка всех входящих сообщений
        except Exception:
             print("Ошибка получения сообщения")

