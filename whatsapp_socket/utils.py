import os
import sys


def IsConsoleVar(var):
    return var in sys.argv


ROOT_PATH = os.path.abspath(__file__).replace('utils.py', '').replace('\\', '/')

MYSQL_HOST = '127.0.0.1'
MYSQL_LOGIN = 'root'
MYSQL_PASSWORD = ''
MYSQL_DATABASE = 'stax'
