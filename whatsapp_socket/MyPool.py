import pymysql
from dbutils.pooled_db import PooledDB
from MyLink import MyLink
from pymysql.cursors import DictCursor


class MyPool:

    def __init__(self, host, login, password, db, maxconnections):
        self.pool = PooledDB(pymysql,
                             1,  # mincached
                             0,  # maxcached
                             int(maxconnections/2),  # maxshared
                             maxconnections,  # maxconnections
                             True,  # blocking
                             0,  # maxusage
                             None,  # setsession
                             True,  # reset
                             None,  # failures
                             4,  # ping
                             cursorclass=DictCursor,
                             host=host,
                             user=login,
                             passwd=password,
                             db=db,
                             port=3306,
                             charset='cp1251')

    def GetDedicated(self) -> MyLink:
        return MyLink(conn=self.pool.dedicated_connection())

    def GetShared(self) -> MyLink:
        return MyLink(conn=self.pool.connection())
