class ChatException(Exception):

    def __init__(self, body: dict):
        self._body = body

    def GetBody(self):
        return self._body

    def __str__(self):
        return self._body['message']