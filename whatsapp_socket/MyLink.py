import pymysql
from pymysql import OperationalError, InterfaceError
from pymysql.cursors import DictCursor


class MyLink:
    """Надстройка над pymysql для работы с базой данных"""

    def __init__(self, host=None, login=None, password=None, db=None, conn=None, charset='cp1251'):

        if conn is not None:
            self.link = conn
            return

        self.link = pymysql.connect(
            host=host,
            user=login,
            password=password,
            db=db,
            charset=charset,
            cursorclass=DictCursor
        )

    def Insert(self, table: str, array: dict) -> int:
        """Добавить запась в таблицу table

          :param table: Имя таблицы в базе
          :type table: str
          :param array: Ассоциативный массив {Поле:Значение}
          :type array: dict

          :rtype: None
          :return:
        """

        self.link.ping(reconnect=True)
        cursor = self.link.cursor()

        keys = ""
        values = ""

        for key in array.keys():
            if keys != "":
                keys += ","
                values += ","

            keys += f"`{key}`"

            value = array[key]

            if value is None:
                values += 'NULL'
            else:
                values += "'" + str(value).replace("'", "\\'") + "'"

        cursor.execute(f"INSERT INTO `{table}` ({keys}) VALUES({values})")
        self.link.commit()

        if not isinstance(cursor, DictCursor):
            cursor = cursor._cursor

        cursor.close()

        return cursor.lastrowid

    def Select(self, table: str, where: str = ""):
        """Вернёт список записей из таблицы удовлетворяющих фильтру

          :param table: Имя таблицы в базе
          :type table: str
          :param where: оператор фильтрации WHERE
          :type where: str

          .. note:: в строку where не нужно включать WHERE

          :rtype: list
          :return: Список строк таблицы table удовлетворящих фильтру WHERE
        """

        self.link.ping(reconnect=True)
        self.link.commit()
        cursor = self.link.cursor()

        cursor.execute(f"SELECT * FROM `{table}` WHERE {1 if where == '' else where}")

        result = []

        if not isinstance(cursor, DictCursor):
            cursor = cursor._cursor

        for row in cursor:
            result.append(row)

        cursor.close()

        return result

    def Update(self, table: str, array: dict, where: str):
        """Отредактировать записи в таблице table удовлетворяющих фильтру

          :param table: Имя таблицы в базе
          :type table: str
          :param array: Ассоциативный массив {поле:новое значение}
          :type array: dict
          :param where: оператор фильтрации WHERE
          :type where: str

          .. note:: в строку where не нужно включать WHERE

          :rtype: None
          :return:
        """

        self.link.ping(reconnect=True)
        cursor = self.link.cursor()

        operator_set = ""

        for key in array.keys():
            if operator_set != "":
                operator_set += ","

            value = array[key]

            if value is None:
                operator_set += f"`{key}`=NULL"
            else:
                value = str(value).replace("'", "\\'")
                operator_set += f"`{key}`='{value}'"

        result = cursor.execute(f"UPDATE `{table}` SET {operator_set} WHERE {1 if where == '' else where}")
        self.link.commit()
        cursor.close()

        return result

    def Delete(self, table, where):
        """Удалить записи из таблицы table удовлетворяющих фильтру

          :param table: Имя таблицы в базе
          :type table: str
          :param where: оператор фильтрации WHERE
          :type where: str

          .. note:: в строку where не нужно включать WHERE

          :rtype: None
          :return:
        """

        cursor = self.link.cursor()
        cursor.execute(f"DELETE FROM `{table}` WHERE {1 if where == '' else where}")

        self.link.commit()
        cursor.close()

    def Close(self):

        self.link.close()

    def CustomQuery(self, query, is_select: bool = False):
        """Свободный запрос к базе данных

          :param query: Строка запроса
          :type query: str
          :param is_select: Является ли запрос типом Select. Если True вернёт список записей таблицы
          :type is_select: bool

          :rtype: list
          :return: Список записей таблицы
        """

        self.link.ping(reconnect=True)
        cursor = self.link.cursor()
        cursor.execute(query)

        self.link.commit()

        if is_select:

            if not isinstance(cursor, DictCursor):
                cursor = cursor._cursor

            result = []

            for row in cursor:
                result.append(row)

            cursor.close()

            return result

        cursor.close()
